use tempdb;

# Create the table (using certain constraints)
create table student (
	student_id int auto_increment,
    name varchar(20) not null,
    major varchar(20) default 'undefined',
    primary key(student_id)
);

# Command to delete the table
drop table student;

# Command to view the table
select * from student;

# Insert command
insert into student(name, major) values('Siddarth', 'Backend'); -- The id's will start from 1 and will get auto increamented
insert into student(name, major) values('Mahesh', 'Devops');
insert into student(name, major) values('Prabhat', 'Frontend');

# insert into student(student_id, name) values(2, 'Mahesh');
# insert into student values(3, 'Prabhat', 'Frontend');

# Updating

update student
set major = 'SDE'
where major = 'Backend';
# Updates the major whose value was "Backend"

update student
set major = 'Data'
where student_id = 3;
# Updates the major feild of the student with student-id = 3

update student
set major = 'SDE'
where major = 'Data' or major = 'Devops';
# Uses the OR condition to update

update student
set name = 'Siddarth', major = 'Data Sci'
where student_id = 1;
# Update the name and major field of the student_id = 1
# If we dont use where keyword then all the cols get updated

# Deleting

delete from student
where student_id = 3;
# Delete the row/entry which has student_id = 3


